# Introduction to Python, Jupyter Notebook and Pandas

This repo contains several notebooks and data that I wrote to introduce [Python][python], [Jupyter notebook][jupyter] and [Pandas][pandas]. It focus on the basic of Jupyter notebook and Pandas and less so on Python itself. The idea is to provide a short cut for those who wish to use [Pandas][pandas] directly. It does not cover any programming basics and should not be used as an introduction to Python. 

There are two main folders namely, notebook and data. The notebook folder contains all the relevant Jupyter notebooks. There are six main notebooks covering the basic of Jupyter, elementary data operation and manipulation using Pandas, a brief introduction to the concept of *split-apply-combine*, an introduciton of data visualisation using [matplotlib][matplotlib] and [seaborn][seaborn] as well as a very quick demo on estimating statistical models using [statsmodels][statsmodels]. 

The data folder contains all the data required for the notebooks to run. Details of the data can be found below.

## notebook

**Yet to complete**

## data

**Yet to complete**

[seaborn]: https://seaborn.pydata.org/
[matplotlib]: https://matplotlib.org/
[jupyter]: https://jupyter.org/
[python]: https://www.python.org/
[pandas]: https://pandas.pydata.org/
